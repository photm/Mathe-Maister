# Mathe-Maister
Ansammlung einiger wichtiger Themenübergreifender mathematischer Definitionen und Sätze, später dann verpackt in Anki in schönem Latex, um die Sprache der Mathematik fließender zu verstehen.

# FAQ
### > Wie komme ich in den Genuss der Mathe-Maister Decks?
- Anki runterladen (zum Beispiel AnkiDroid im Play Store). Damit hat man die
  Software, die mit den erstellten Karten etwas anfangen kann. Das Programm ist
  anfangs komplet leer, da Anki selbstverständlich nicht weiß, was man lernen
  will!
- Gewünschte Decks runterladen und importieren. Alle mathematischen Inhalte,
  meine Designentscheidungen zum Lernerlebnis, und LaTeX-Support (braucht
  bedingt Internet, siehe unten), sind schon enthalten.
- einfach loslernen und täglich dranbleiben

### > Wo finde ich die Decks?
Hier: https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Anki-Decks

### > Wo kann ich Anki runterladen?
* für den PC: https://apps.ankiweb.net/
* für Android: AnkiDroid auf
  [F-Droid](https://f-droid.org/packages/com.ichi2.anki/) oder im [Play
  Store](https://play.google.com/store/apps/details?id=com.ichi2.anki)
* für iOS: weiß nicht, aber da gibts was. ACHTUNG: Bisher wurden keine Tests auf
  iOS Anki durchgeführt!

### > Wie bleibe ich auf dem Laufenden zu großen Updates?
Dafür gibt es [diesen E-Mail-Verteiler](http://supersecret.me/ankiprojekt/), auf dem man sich jederzeit ein- und austragen kann.

### > Wo kann ich mir die Inhalte der Karteikarten auf einem Blick ansehen?
Hier sind die Links zu für Menschen schön lesbaren PDFs aller laufenden Stoffsammlungen. Inhaltliche Korrektheit ohne Gewähr:
+ [Lineare Algebra I (läuft, bald fast fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.pdf)
+ [Analysis III (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.pdf)
+ [Geometrie I (pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.pdf)
+ [Numerik I (läuft, fast fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.pdf)

### > Warum gibt es das?
Vor allem  nach Abschließen  einer Vorlesung vergisst  man ständig  die formale,
genaue Formulierung einiger grundlegender Definitionen  und Sätze. Da das leider
fast jedem zu oft passiert, und vor allem  mir auf den Keks geht, habe ich schon
seit  dem ersten  Semester den  Traum, welchen  ich jetzt  verwirklichen möchte:
Karteikarten gestütztes  Verfrachten aller gelernten Definitionen  und wichtiger
Sätze ins Langzeitgedächtnis, umgesetzt  durch schöne, strukturierte, effiziente
Decks in Anki.

### > Warum Karteikarten / Warum Anki?
Karteikarten (aus Papier) an sich sind schon ganz schick fürs sture
Auswendiglernen von 1 zu 1 verknüpfbarem Wissen, da es pro Karte eben nur zwei
Seiten gibt und man von der einen jeweils auf die andere Karte kommen muss. Das
ist auch schon die einzige Limitierung, weswegen diese Decks auch nicht dafür
geeignet sind, Konzepte neu zu erlernen, sondern schon gelernte Konzepte nicht
zu vergessen, indem man zum einen, nach eigenem Ermessen, streng die genaue
Formulierung wiederholt, oder zumindest daran erinnert wird, an das vorliegende
Konzept zu denken. Damit sind wir auch schon bei der Stärke von Anki. 
Anki funktioniert nach dem Spaced Repetition System (SRS), nach welchem einem eine
Karte in größer werdenden Abständen gezeigt wird, idealerweise genau kurz bevor
die Erinnerungen daran schwammig werden. So schaut man sich eine Karte nicht
öfter an, als nötig, aber noch bevor man sie vergisst. Je nachdem, wie gut man
sich dann an eine Karte erinnern konnte, so wird sie passend geplant für ihren
nächsten Auftritt. Anders als bei echten Karteikarten, sortiert Anki also die
Karten passend und legt einem täglich das vor, was man gerade zu vergessen droht
(+ neue Karten, die man heute zum ersten Mal sieht). Natürlich bleibt auch das
viele, schwere Papier weg und jede Karte ist beliebig voll befüllbar, in
teilweise extra anklickbaren Extra-Feldern. Und andere Funktionen, die für uns
nicht relevant sind. Am allerbesten ist jedoch natürlich, dass Anki, bzw
AnkiDroid (für Android, zu finden im Play Store) kostenlos ist (außer für iOS
Geräte, da kostet es bisschen viel), und dass Decks und Karten ganz einfach
verteilt und verwaltet werden können, inklusive kostenlosem Account bei Anki,
mit dem man zwischen verschiedenen Geräten synchronisieren kann.

### > Wie funktioniert Anki, was kann ich da alles machen?
Wirklich wichtige Funktionen für dieses Projekt beschreibe ich im Folgenden, den
Rest kann man sich selbst aneignen durch Rumprobieren, auf
https://apps.ankiweb.net/docs/manual.html Nachschauen, Video-Tutorials
Anschauen, Blog Posts dazu Lesen, oder mich Fragen.

### > Wie kann ich ein runtergeladenes Deck in Anki reinstecken/importieren?
- Am PC genügt es normalerweise, nachdem Anki installiert wurde, einfach auf die
  Datei vom Deck  (sollte als Endung `.apkg` haben)  doppelt zu klicken. Alternativ per
  Drag&Drop in Anki reinziehen. Oder Anki  öffnen und in der Menüleiste auf File
  -> Import... und da dann das Deck auswählen und öffnen
- Am Handy reicht es normalerweise auch, die Datei runterzuladen und dann zu
  öffnen. Bei mir zumindest wird es dann ohne Probleme von Anki erkannt und
  importiert. Ich empfehle aber, alle Decks am PC zu importieren und dann durch
  Synchronisierung aufs Handy zu bekommen. Ist aber reine Geschmackssache.

### > Wie füge ich neue Karten zu meinem bestehenden Deck hinzu?
(nur benötigt, wenn das runtergeladene Deck noch nicht die fertige Version ist, also wenn das Deck mit einem Datum ausgestattet ist)
(Siehe auch stattdessen weiter unten die Anleitung zum CSV-Import, falls nicht ungeschickt mit Computern)   
Sobald man sich zum Beispiel `Lineare Algebra I 18-11-30` runtergeladen hat,
also alle LA1 Karten von Anfang der Vorlesung bis zum 30.11.2018, sollte man
sich nicht einfach die nächste Version des Decks runterladen, da man ja sonst
das alte Deck hat mit dem Lernfortschritt und dann nochmal alle diese Karten im
neuen Deck und ein paar mehr. Dadurch würde man alle alten Karten jetzt doppelt
haben, später noch öfter, und das würde das ganze SRS zunichte machen.
Stattdessen sollte man sich das passende Ergänzungsdeck (ED) (vom Konzept her
für Anki nichts anderes als ein normales Deck, aber von mir nur ausgestattet mit
den Karten, die einem fehlen) runterladen, gekennzeichnet durch ein x und einen
Zeitraum, hier zum Beispiel `Lineare Algebra 1 x 18-11-30 bis 18-12-06`, und in
Anki importieren. Nun hat man das alte Deck und das ED mit allen aktuellen
Karten. Am besten auf dem PC, also nicht am Handy, schiebt man jetzt alle Karten
vom ED in das alte Deck. Dafür auf Anki auf dem PC (vorher auf dem Handy und auf
dem PC synchronisieren!) auf das ED klicken, dann oben auf "Browse", dann
einfach ENTER drücken. Jetzt sollten alle Karten des EDs angezeigt werden. Hier
alle Karten markieren und dann oben in der Menüleisten auf Cards -> Change Deck
und hier das alte Deck auswählen. Schon sind alle neuen Karten aus dem ED im
alten Deck.

### > Was passiert, wenn die Karten, die schon veröffentlicht wurden, überarbeitet werden?
Im Falle von Fehlern oder sonstigen Unschönheiten werden die Karten in allen
zukünftigen Veröffentlichungen angepasst. Es gibt auch eine einfache Methode, verbesserte Fehler ganz einfach in schon benutzten Decks zu beseitigen, wofür wir leicht ausholen:
Die Decks in Anki funktionieren im Grunde wie Datenbanken. Jede Notiz hat als eindeutige Identifizierung ihren ersten Eintrag. Im Falle der Mathe-Maister-Notizen ist das das Feld "Name". Dadurch können auch keine zwei Notizen exisiteren, die im ersten Feld den genau gleichen Inhalt haben. Solange also keine Verbesserungen am ersten Feld von alten Notizen vorgenommen wurden, können mit der im Folgenden beschriebenen Methode alle Felder auf den neusten Stand gebracht werden, ohne dass ihr euren Lernfortschritt verliert. Persönlich von euch bearbeitete Felder gehen dabei natürlich verloren. 
Bevor man jedoch folgende Methode verwendet, sollte man sicherstellen, ob die ersten Felder aller Notizen im eigenen Deck übereinstimmen mit den ersten Feldern in der aktuelleren Version, da sonst für jede Notiz mit unterschiedlichen ersten Feldern eine neue Notiz angelegt wird. Änderungen an ersten Feldern von Notizen können den Changelogs entnommen werden: https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Changelog

### (optional) Karten Aktualisieren und Hinzufügen durch CSV-Import
* `CSV`-Datei zur passenden Vorlesung in der [Stoffsammlung](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Stoffsammlung) runterladen
* in Anki auf das zu aktualisierende Deck klicken, dann oben links *File* → *Import...* und die Datei auf eurem Computer öffnen
* ein neues Fenster ist in Anki offen, hier auf ein paar wichtige Sachen achten:
	* Ganz oben links bei `Type` den Notiztyp *Mathe-Maister Definitionen* (könnte leicht abweichen) wählen
	* Daneben bei `Deck` das Deck, in dem ihr die Karten aktualisieren oder hinzufügen wollt
	* Gleich darunter `Fields seperated by: Tab` auswählen
	* Direkt darunter `Update existing notes when first field matches` wählen 
	* Direkt darunter Das Häkchen ☑ setzen bei `Allow HTML in fields`
	* Alles hierunter, also die Feldzuweisungen, sollten stimmen
	* ganz unten auf *Import* klicken
* in einem Ergebnisfenster seht ihr, wie viele Karten hinzugefügt und verändert wurden

### > Meine Karten werden nicht richtig angezeigt, ich sehe nur unleserliche Zeichen und Brabbeleien
Auf dem Handy einfach kurz Handy anmachen, Deck nochmal neu öffnen, dann sollte
es funktionieren, bis zum kompletten Schließen der App. Dann braucht man wieder
kurz Internet, damit der Code kurz laden kann. Auf Linuxmint scheint es zu
funktionieren. Auf Windows PCs funktioniert es bisher nicht, das wird aber von
mir auch nicht in naher Zukunft behoben.

## Special Thanks
+ Felix W :owl: (Early Testing)
+ Ingo Blechschmidt :turtle: (Korrektur, Motivation)
+ Julia G :bird: (Lineare Algebra I)
+ Kilian R :unicorn: (Readme-Layout-Spezialist, Optimierungs-Enthusiast, Blockchain Entrepreneur)
+ Leonie N :shark: (Motivation, Beratung) 
+ Michael Struwe :frog: (Skripte, CI, git Setup)
+ Moritz H :wolf: (Numerik I, Marketing)


Kontakt: alex.m.s@gmx.de
