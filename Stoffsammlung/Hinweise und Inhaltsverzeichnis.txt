Inhaltsverzeichnis, neue Einträge bitte möglichst passend einordnen (falls keine Ahnung, einfach in 0.0 rein, wird dann von jemandem einsortiert):
In Klammern jeweils Estimated Time of Arrival und Hauptautor/Verantwortlicher, wenns einen gibt.
0.0 Mathe Basics
1.0 Analysis
   1.1 Analysis I (ETA: SoSe19)
   1.2 Analysis II (ETA: N/A)
   1.3 Analysis III (läuft, Alex)
2.0 Algebra
   2.1 Lineare Algebra I (läuft, Alex)
   2.2 Lineare Algebra II (ETA: SoSe19)
   2.3 Algebra I (ETA: N/A)
   2.4 Algebra II (ETA: N/A)
3.1 Geometrie I (läuft, Alex~ / Milan?)
4.1 Numerik I (läuft, eher nur Formelsammlung, Alex / Moritz)
5.1 Optimierung I (ETA: März 2019)


Wichtige Konventionen für dieses Projekt bezüglich Sonderzeichen etc:
 - in den CSV-Dateien werden folgende Zeichen ersetzt durch ihre HTML-Befehle, damit der Import nach Anki (verwendet HTML) problemlos funktioniert. Diese Sonderzeichen werden bei der Erzeugung der PDFs verarbeitet, müssen also hinterher nie wieder bearbeitet werden:
		&gt statt >
		&lt statt <
		<br/> statt ZEILENUMBRUCH bzw statt ENTER bzw statt einer neuen Zeile
 - Funktionennamen werden im Mathe-Modus passend markiert. Während bekannte Funktionen wie sin durch \sin entsprechend angepasst werden (also einfach durch ein "\"), müssen nicht allgemein bekannte Funktionen lokal zu Mathe-Operatoren gemacht werden:
	Ist kein Umlaut in der Funktionsbezeichnung, so reicht \operatorname{fantasiefunktion}.
	Enthält sie Umlaute, so wird \mathrm{fünktiön} verwendet, wobei jetzt der Abstand direkt danach eventuell zu klein ausfällt und manuell hinzugefügt werden muss
 - keine griechischen Buchstaben verwenden, nur Zeichen aus ASCII und Umlaute und so lala Kram
 - Sonstige erwähnenswerte zu beachtende Schreibweisen:
		^{\circ} statt °
 - Stilentscheidungen (bitte auch beachten)_
		\int \limits_ statt \int (gleiches für \prod, \sum etc)
		\sin statt sin (gleiches für andere bekannte mathematische Operatoren)
		\operatorname{Rang}(x) statt Rang(x) (gleiches für andere nicht in mathjax bekannte mathematische Operatoren)
		\ast statt *
		\dots statt ...


Ungefähr an folgendes Format halten mit diesen Hinweisen:
 - den Namen nicht in der Beschreibung erwähnen, Name immer mit großem Buchstaben anfangen, auch wenn es ein Adjektiv ist
 - Wohin was gehört, kann man der letzten Zeile entnehmen, da stehen die Namen der einzelnen Felder (diese Zeile muss vor dem Import in Anki gelöscht werden!)
 - Pflichtfelder sind eigentlich nur die ersten 2; Name und Beschreibung. Auch noch wichtig sind Typ, Themenbereich und Vorlesung. Alle danach sind optional
 - Feld "Name" beginnt immer groß, alle anderen Felder klein, wenn kein Nomen (also normale Rechtschreibung)


Reihenfolge der Felder:
Name
Beschreibung
X Notizen
Vorlesung
Typ
Themenbereich
Formel
Interpretation
FunFacts
Trivia
Beispiele
Lesehinweis
Extras
Literaturhinweis
Übersetzung
X Lernhinweis (F)
X Lernhinweis (B)

zum Import in die CSV-Datei zum Abgleichen:
Name	Beschreibung	X Notizen	Vorlesung	Typ	Themenbereich	Formel	Interpretation	FunFacts	Trivia	Beispiele	Lesehinweis	Extras	Literaturhinweis	Übersetzung	X Lernhinweis (F)	X Lernhinweis (B)
name	description	xnotes	lecture	type	topic	formula	interpretation	funfacts	trivia	examples	readinghint	extras	literature	translation	xhintfront	xhintback


 

Noch zu erledigen:
 - := durch (in Anki bzw Mathjax darstellbares) gescheites := ersetzen mit zentriertem Doppelpunkt
 - bessere Lösung finden für Operatornamen mit Umlauten statt mathrm
