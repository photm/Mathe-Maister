#!/bin/sh
# Hashtag zum kommentieren, aber erste Zeile ist ein Kommentar, sagt, dass sh verwendet werden soll, liegt in bin
# jetzt sagen, wo die .csv liegt, entweder reingeben, oder Pfad geben
# $1 hei�t, hier wird das 1. Argument genommen
##cat $1
# progA | progB gibt die Ausgabe von A als EIngabe in B rein, f�hrt B dann auch aus
# while f�r alle Zeilen, ; und ENTER ungef�hr gleich

texechon() {
	#$* ist wie $1 $2 $3 $4 .........
	#printf nimmt als erstes Argument Anweisungen, was als n�chstes kommt, danach eigentlich die auszugebenden Sachen
	printf "%s\\\\\\\\\n" "$*"
}

texecho() {
	# texechon aber ohne neue Zeile am Ende
	printf "%s" "$*"
}

optechon() {
	# f�r optionale Zeile, pr�ft erstmal, ob sie nicht leer sind. $1 ist das Textk�rzel am Anfang der Zeile
	[ -n "$2" ] && texechon "$1: $2"
}

optecho() {
	# optechon ohne neue Zeile am Ende, leicht anders
	# printet auch nur, wenn das zweite Argument nicht leer ist, aber nutze 1 Argument davor bzw beliebig viele danach f�r Text davor und dahinter
	[ -n "$2" ] && texecho "$*"
}

# Kopf des Latex-Dokuments
printf "%s" "\documentclass{article}

\usepackage{amsmath,amsthm,amssymb,amscd,stmaryrd,color,graphicx,environ,tabto}
\usepackage[utf8]{inputenc}

\begin{document}

Korrekturlese-/Ueberblicksversion einer Vorlesung aus der Mathe-Maister-Stoffsammlung. Infos und mehr unter \\textbf{gitlab.com/CptMaister/Mathe-Maister} \\\\\\\\
"


##exit # beendet das Skript hier
# CSV Datei ist hier schon als Standardinput gepipet
# -r verschont alle "\"
while read -r line
	do
	#read next line if first letter is '#'
	[ "$(echo "$line" | cut -c1)" = "#" ] && continue
	[ "$(echo "$line" | cut -c1-28)" = "Hallo Freund der Mathematik!" ] && continue
	
	#printf "%s\n" "$line"
	# read liest immer eine Zeile (:=line), dann mit cut immer die durch TAB getrennten Felder i (-fi) rauslesen und in Variablen setzen
	name="$(texecho "$line" | cut -f1)"
	description="$(texecho "$line" | cut -f2)"
	#xnotes="$(texecho "$line" | cut -f3)"
	#lecture="$(texecho "$line" | cut -f4)"
	type="$(texecho "$line" | cut -f5)"
	topic="$(texecho "$line" | cut -f6)"
	formula="$(texecho "$line" | cut -f7)"
	interpretation="$(texecho "$line" | cut -f8)"
	funfacts="$(texecho "$line" | cut -f9)"
	trivia="$(texecho "$line" | cut -f10)"
	examples="$(texecho "$line" | cut -f11)"
	readinghint="$(texecho "$line" | cut -f12)"
	extras="$(texecho "$line" | cut -f13)"
	literature="$(texecho "$line" | cut -f14)"
	translation="$(texecho "$line" | cut -f15)"
	#xhintfront="$(texecho "$line" | cut -f16)"
	#xhintback="$(texecho "$line" | cut -f17)"
	# echo ist dann der Standardoutput meines Skripts
	
	# hier dann im gew�nschten Format ausgegeben mit texecho, echo mit latex-Zeilenumbruch \\ am Ende
	texecho "\textbf{$name} ($type"
		optecho "" "$literature"
		texechon "; $topic)"
	texechon "$description"
	# ab hier eventuell optionale Zeilen, nur wenn in dem Feld was steht, daf�r "test -n TEXT" benutzen, gibt TRUE wenn TEXT nonzero ist, hacken f�r elegante if, bzw andere Schreibweise [ -n TEXT ]
	optechon "F" "$formula"
	optechon "I" "$interpretation"
	optechon "FF" "$funfacts"
	optechon "T" "$trivia"
	optechon "e.g." "$examples"
	optechon "Hint" "$readinghint"
	optechon "Extra" "$extras"
	optechon "Lang" "$translation"
	
	#f�r zwei Leerzeilen (brauche 4mal so viele backslashes wie sonst):
	echo "\\\\\\\\\\\\\\\\"
	
done

# Latex-Dokument schlie�en
printf "
\end{document}"


# cmd-Befehl f�r SKRIPT <(nimmt) CSV >(gibt aus) TEXDATEI
# /home/bimmler/Desktop/Ankimathe/ankiskript/fixHTMLinTEX.sh < /home/bimmler/Desktop/Ankimathe/ankiskript/convertCSVtoTEXAnkimathe.sh < /home/bimmler/Desktop/Ankimathe/ankiskript/LA1.csv > /home/bimmler/Desktop/Ankimathe/ankiskript/latextest.tex

